export const environment = {
  production: true,
  apiUrl: 'http://10.240.74.12:5000/store/v1',
  formatDate: 'DD/MM/YYYY',
  formatDateDB: 'YYYY-MM-DD',
};
