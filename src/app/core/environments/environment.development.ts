export const environment = {
    production: false,
    apiUrl:'http://10.240.74.12:6065/store/v1',
    formatDate: 'DD/MM/YYYY',
    formatDateDB: 'YYYY-MM-DD'
};
