import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Product } from 'src/app/core/model/product';
import { ProductsService } from '../../services/products.service';
import { AccordionTab } from 'primeng/accordion';
import { Category } from 'src/app/core/model/category';
import { CategoriesService } from 'src/app/modules/categories/services/categories.service';
import { getSelectedTags } from 'src/app/core/utils/filter';

@Component({
  selector: 'app-panelfilter',
  templateUrl: './panelfilter.component.html',
  styleUrls: ['./panelfilter.component.scss']
})
export class PanelfilterComponent {
  public form: FormGroup;
  public categories: Category[] = [];
  public selectedTags: Map<string, any> = new Map<string, any>();

  constructor(
    private productsService: ProductsService,
    private categoriesService: CategoriesService
  ) {
    this.form = this.createFormGroup();
    this.loadCategories();
  }

  private createFormGroup() {
    return new FormGroup({
      name: new FormControl('', { nonNullable: true }),
      categoryId: new FormControl('', { nonNullable: true })
    });
  }

  private loadCategories() {
    this.categoriesService
      .findAll()
      .subscribe({
        next: (response) => this.categories = response
      });
  }

  public clear() {
    this.form.reset();
    this.selectedTags.clear();
  }

  public search(tab: AccordionTab, event: MouseEvent | KeyboardEvent) {
    const data: Product = {
      ...this.form.value,
    };
    this.productsService.setObjectFilterChange(data);
    this.selectedTags = getSelectedTags(document, 'formProduct');
    tab.toggle(event);
  }
}
