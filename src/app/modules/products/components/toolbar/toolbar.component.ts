import { Component } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { ModalformsComponent } from '../modalforms/modalforms.component';
import { TableComponent } from '../table/table.component';
import { ModalinfoComponent } from 'src/app/shared/components/modalinfo/modalinfo.component';
import { ModaldeleteComponent } from 'src/app/shared/components/modaldelete/modaldelete.component';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {
  private modalformsComponent!: ModalformsComponent;
  private modalinfoComponent!: ModalinfoComponent;
  private modaldeleteComponent!: ModaldeleteComponent;
  private tableComponent!: TableComponent;

  constructor(
    private productsService: ProductsService
  ) { }

  ngOnInit() {
    this.productsService.trigger.subscribe((modalformsComponent) => {
      this.modalformsComponent = modalformsComponent;
    });

    this.productsService.triggerInfo.subscribe((modalinfoComponent) => {
      this.modalinfoComponent = modalinfoComponent;
    });

    this.productsService.triggerMessages.subscribe((modaldeleteComponent) => {
      this.modaldeleteComponent = modaldeleteComponent;
    });

    this.productsService.triggerTable.subscribe((tableComponent) => {
      this.tableComponent = tableComponent;
    });
  }

  public info() {
    this.modalinfoComponent.openInfo();
  }

  public deleteSelectedProducts() {
    this.modaldeleteComponent.openConfirm();
  }

  public create() {
    this.modalformsComponent.openCreate();
  }

  public edit() {
    this.modalformsComponent.openEdit();
  }
}
