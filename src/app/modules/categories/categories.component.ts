import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs';
import { Category } from 'src/app/core/model/category';
import { CategoriesModule } from './categories.module';
import { CategoriesService } from './services/categories.service';
import { ModalinfoComponent } from 'src/app/shared/components/modalinfo/modalinfo.component';
import { ModaldeleteComponent } from 'src/app/shared/components/modaldelete/modaldelete.component';
@Component({
  selector: 'app-categories',
  standalone: true,
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  imports: [CategoriesModule, ModalinfoComponent, ModaldeleteComponent],
})
export default class CategoriesComponent implements OnInit {
  public categories!: Category[];
  public category!: Category;

  constructor(public categoriesService: CategoriesService) {}

  ngOnInit(): void {
    this.createGrid();
    this.categoriesService.getObjectFilterChange().subscribe(() => {
      this.createGrid();
    });
    this.retrieveObjectSelection();
  }

  private retrieveObjectSelection() {
    this.categoriesService
      .getObjectSelectedChange()
      .subscribe((response: Category) => {
        this.category = response;
      });
  }

  createGrid() {
    this.categoriesService
      .findAll()
      .pipe(tap((items: Category[]) => (this.categories = items)))
      .subscribe();
  }
}
