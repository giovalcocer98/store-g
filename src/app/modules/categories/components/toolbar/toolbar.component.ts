import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Category } from 'src/app/core/model/category';
import { CategoriesService } from '../../services/categories.service';
import { ModalformsComponent } from '../modalforms/modalforms.component';
import { TableComponent } from '../table/table.component';
import { ModalinfoComponent } from 'src/app/shared/components/modalinfo/modalinfo.component';
import { ModaldeleteComponent } from 'src/app/shared/components/modaldelete/modaldelete.component';
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  private modalformsComponent!: ModalformsComponent;
  private modalinfoComponent!: ModalinfoComponent;
  private modaldeleteComponent!: ModaldeleteComponent;
  private tableComponent!: TableComponent;
  public items: MenuItem[] = [];
  public cardMenu: MenuItem[] = [];
  public categorySelectedTable: any;
  public categoryDialog: boolean = false;
  public categoryDialogDelete: boolean = false;
  public categoryDialogInfo: boolean = false;
  public submitted: boolean = false;
  public category = new Category();
  public data: object = {};
  public date: Date | undefined;

  constructor(private categoriesService: CategoriesService) {}

  ngOnInit() {
    this.items = [
      { label: 'Duplicate', icon: 'pi pi-clone', url: 'http://angular.io' },
      { label: 'Print', icon: 'pi pi-print', routerLink: ['/theming'] },
    ];

    this.cardMenu = [
      {
        label: 'Save',
        icon: 'pi pi-fw pi-check',
      },
      {
        label: 'Update',
        icon: 'pi pi-fw pi-refresh',
      },
      {
        label: 'Delete',
        icon: 'pi pi-fw pi-trash',
      },
    ];

    this.categoriesService.trigger.subscribe((modalformsComponent) => {
      this.modalformsComponent = modalformsComponent;
    });

    this.categoriesService.triggerInfo.subscribe((modalinfoComponent) => {
      this.modalinfoComponent = modalinfoComponent;
    });

    this.categoriesService.triggerMessages.subscribe((modaldeleteComponent) => {
      this.modaldeleteComponent = modaldeleteComponent;
    });

    this.categoriesService.triggerTable.subscribe((tableComponent) => {
      this.tableComponent = tableComponent;
    });
  }

  public create() {
    this.modalformsComponent.openCreate();
  }
  public edit() {
    this.modalformsComponent.openEdit();
  }
  public deleteSelected() {
    this.modaldeleteComponent.openConfirm();
  }
  public info() {
    this.modalinfoComponent.openInfo();
  }
}
