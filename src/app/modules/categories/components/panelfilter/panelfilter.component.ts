import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AccordionTab } from 'primeng/accordion';
import { Category } from 'src/app/core/model/category';
import { CategoriesService } from '../../services/categories.service';
import { getSelectedTags } from 'src/app/core/utils/filter';

@Component({
  selector: 'app-panelfilter',
  templateUrl: './panelfilter.component.html',
  styleUrls: ['./panelfilter.component.scss'],
})
export class PanelfilterComponent {
  public categories!: Category[];
  public category!: Category;
  public submitted: boolean = false;
  public formCategory: FormGroup;
  public selectedTags: Map<string, any> = new Map<string, any>();

  constructor(private categoriesService: CategoriesService) {
    this.formCategory = this.createFormGroup();
  }

  public search(tab: AccordionTab, event: MouseEvent | KeyboardEvent ) {
    const data: Category = {
      ...this.formCategory.value,
    };
    this.categoriesService.setObjectFilterChange(data);
    this.selectedTags = getSelectedTags(document, 'formCategory');
    tab.toggle(event);
  }

  private createFormGroup() {
    return new FormGroup({
      name: new FormControl('', { nonNullable: true }),
      description: new FormControl('', { nonNullable: true }),
    });
  }

  public clear(){
    this.formCategory.reset("");
    this.selectedTags.clear();
  }
}
