import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { PrimeModule } from 'src/app/prime.module';
import { CategoryRoutingModule } from './categories-routing.module';
import { ModalformsComponent } from './components/modalforms/modalforms.component';
import { PanelfilterComponent } from './components/panelfilter/panelfilter.component';
import { TableComponent } from './components/table/table.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { FilterInformationComponent } from "../../shared/components/filterinformation/filterinformation.component";

@NgModule({
  declarations: [
    TableComponent,
    ToolbarComponent,
    PanelfilterComponent,
    ModalformsComponent,
  ],
  exports: [
    TableComponent,
    ToolbarComponent,
    PanelfilterComponent,
    ModalformsComponent,
  ],
  imports: [
    CommonModule,
    PrimeModule,
    CategoryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    FilterInformationComponent,
  ],
})
export class CategoriesModule {}
