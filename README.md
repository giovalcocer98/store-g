# Store Frontend

## Angular Enterprise Folder Structure

Una estructura de proyecto empresarial en Angular suele tener características que la hacen organizada, escalable y fácil de mantener.

![enterprise_folder_structure](https://gitlab.et.bo/angular-demos/angular-cart-demo/-/raw/master/src/assets/img/FolderStructure.png)

> Fuente: https://malcoded.com/

- **Core.-** Un módulo central que contiene servicios compartidos, interceptores HTTP, guardias de rutas y otros elementos que son esenciales para toda la aplicación.
- **Shared.-** Contiene componentes, directivas y módulos reutilizables que pueden ser utilizados en varios lugares de la aplicación. Esto promueve la consistencia visual y funcional.
- **Feature.-** Cada carpeta representa un módulo funcional de la aplicación, que contiene sus propios componentes, servicios y otros archivos necesarios.

La idea es seguir una organización modular y escalable que facilite el desarrollo y mantenimiento del código a medida que el proyecto crece.
```
src/
|-- app/
|   |-- core/
|   |   |-- constants/
|   |   |-- directives/
|   |   |-- enums/
|   |   |-- environments/
|   |   |-- guards/
|   |   |-- interceptors/
|   |   |-- interfaces/
|   |   |-- layout/
|   |   |-- model/
|   |   |-- pipes/
|   |   |-- services/
|   |   |-- utils/
|   |
|   |-- shared/
|   |   |-- components/
|   |   |-- modules/
|   |
|   |-- modules/
|   |   |-- feature1/
|   |   |   |-- components/
|   |   |   |-- services/
|   |   |   |-- feature1.module.ts
|   |   |
|   |   |-- feature2/
|   |   |-- ...
|   |
|   |-- app.component.html
|   |-- app.component.scss
|   |-- app.component.ts
|   |-- app.module.ts
|
|-- assets/
|
|-- environments/
|
|-- index.html
|-- main.ts
|-- styles.scss
|-- ...
```